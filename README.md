# eyie - A Stupid KeyBinder

## What is it ?

It does the same thing sxhkd or xbindkeys does:
associate key strokes to actions.

### What's different with it ?

- No configuration file, like the suckless softwares
- Few SLOCs

### How can it be useful ?

The goal is to have a crystal-clear code, especially
regarding the configuration part, so that
you can extend eyie with your own hooks directly written in C,
without the need to call an external command.

## Where is it going ?

### Goals / TODOs

In this order:

- Comment things
- Have a clean code
- Try to be lightweight
- Try to be portable (at least BSDs would be great)

### Non Goals

- Support configuration file
- Support scripting language
- Support non UNIX environment
- Support events other than keyboard press (mouse mouvements for example)

## Building

### Requirements

- XCB header files
- X11 header files (needed for key symbols)
- libc (obviously, right ?)

### How to build

`${CC} -lxcb -lxcb-keysyms eyie.c -o eyie`

### Side note

tcc makes a smaller executable than gcc in this case, even with `-Os`.

## Inspirations

- xbindkeys, sxhkd
- dwm

## License

ISC


## Contributing

Yes you can !
If you want your revolutionary fork to be linked here, drop me an issue.

