/* The CMD variadic macro allows you to pass a
 * command to the spawn function without headaches.
 * Remember that each argument is separated from
 * the others with a comma. If you forget it,
 * the spawned command will be lost.
 *
 * For example, the argument to pass to the spawn function
 * in order to launch "ls -l /" is:
 * CMD("ls", "-l", "/")
 *
 * Please note that the spawn function uses execve
 * hunder the hoods, so you have to pass an absolute path.
 * This is not shell invocation. Consequently,
 * everything with shell expressions, such as "~/<something>"
 * won't work with CMD.
 *
 * That's why SHCMD is proposed here. It's basically
 * an encapsulation of your command in a shell.
 */
#define CMD(...) { .v = (const char*[]){ __VA_ARGS__, NULL } }
#define SHCMD(arg) CMD("/bin/sh", "-c", arg)

/* Each hotkey in this array contains, in this order:
 * 1) One or several modifier keys (OR them to have multiple)
 * 2) One "primary" key
 * 3) The keyboard event which fires this hotkey
 * 4) The function to call
 * 5) The argument to pass to the function
 *
 * 4 is currently limited to spawn, but you can extend it
 * if you want eyie to do something directly.
 *
 * 3 is limited to {XCB_KEY_PRESS, XCB_KEY_RELEASE} for now.
 * The names are self explanatory.
 *
 * 2 must be a valid key symbol defined in the X11 headers
 * (/usr/include/X11/keysymdef.h and /usr/include/X11/XF86keysym.h).
 *
 * 1 must be a valid key symbol modifier defined in /usr/include/xcb/xproto.h:
 * XCB_MOD_MASK_SHIFT
 * XCB_MOD_MASK_LOCK
 * XCB_MOD_MASK_CONTROL
 * XCB_MOD_MASK_1 (Alt)
 * XCB_MOD_MASK_2 (NumLock)
 * XCB_MOD_MASK_3 (CapsLock)
 * XCB_MOD_MASK_4 ("Super" (windows/launch key))
 * XCB_MOD_MASK_5 (Scroll)
 * (Please don't) XCB_MOD_MASK_ANY (One of the above)
 * /!\ Avoid the use of XCB_MOD_MASK_ANY for now,
 * it looks like eyie doesn't catch any key with this modifier /!\
 */
hotkey hotkeys[] = {
/* = Brightness */
{ 0, XF86XK_MonBrightnessUp, XCB_KEY_PRESS , spawn, SHCMD("~/blu.sh") },
{ 0, XF86XK_MonBrightnessDown, XCB_KEY_PRESS , spawn, SHCMD("~/bld.sh") },
/* = Audio = */
/* == Master == */
{ 0, XF86XK_AudioRaiseVolume, XCB_KEY_PRESS, spawn, CMD("/usr/bin/amixer", "-q", "set", "Master", "2%+") },
{ 0, XF86XK_AudioLowerVolume, XCB_KEY_PRESS, spawn, CMD("/usr/bin/amixer", "-q", "set", "Master", "2%-") },
{ 0, XF86XK_AudioMute, XCB_KEY_PRESS, spawn, CMD("/usr/bin/amixer", "-q", "set", "Master", "toggle") },
/* == Headphones == */
{ XCB_MOD_MASK_CONTROL, XF86XK_AudioRaiseVolume, XCB_KEY_PRESS, spawn, CMD("/usr/bin/amixer", "-q", "set", "Headphone", "2%+") },
{ XCB_MOD_MASK_CONTROL, XF86XK_AudioLowerVolume, XCB_KEY_PRESS, spawn, CMD("/usr/bin/amixer", "-q", "set", "Headphone", "2%-") },
{ XCB_MOD_MASK_CONTROL, XF86XK_AudioMute, XCB_KEY_PRESS, spawn, CMD("/usr/bin/amixer", "-q", "set", "Headphone", "toggle") },
/* == Speakers == */
{ XCB_MOD_MASK_SHIFT, XF86XK_AudioRaiseVolume, XCB_KEY_PRESS, spawn, CMD("/usr/bin/amixer", "-q", "set", "Speaker", "2%+") },
{ XCB_MOD_MASK_SHIFT, XF86XK_AudioLowerVolume, XCB_KEY_PRESS, spawn, CMD("/usr/bin/amixer", "-q", "set", "Speaker", "2%-") },
{ XCB_MOD_MASK_SHIFT, XF86XK_AudioMute, XCB_KEY_PRESS, spawn, CMD("/usr/bin/amixer", "-q", "set", "Speaker", "toggle") },
/* = Application launch = */
{ XCB_MOD_MASK_4, XK_u, XCB_KEY_PRESS , spawn, CMD("/usr/bin/urxvt") },
{ XCB_MOD_MASK_4, XK_e, XCB_KEY_PRESS , spawn, CMD("/usr/bin/medit") },
{ XCB_MOD_MASK_4, XK_Pause, XCB_KEY_PRESS , spawn, CMD("/usr/local/bin/slock") },
{ XCB_MOD_MASK_4, XK_b, XCB_KEY_PRESS , spawn, CMD("/usr/bin/brave") },
{ XCB_MOD_MASK_4, XK_v, XCB_KEY_PRESS , spawn, CMD("/usr/bin/vimb") },
{ XCB_MOD_MASK_4, XK_x, XCB_KEY_PRESS , spawn, CMD("/usr/bin/xkill") },
{ 0, XK_Print, XCB_KEY_PRESS , spawn, SHCMD("maim ~/images/$(date +%F-%T).png") },

};
