#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>
#include <xcb/xcb_event.h>
#include <X11/keysym.h>
#include <X11/XF86keysym.h>

typedef union {
	int i;
	unsigned int ui;
	float f;
	const void *v;
} Arg;

typedef struct {
	uint16_t modfield;
	xcb_keysym_t key;
	uint8_t fire_event;
	void (*func)(const Arg *);
	const Arg arg;
} hotkey;

static void grab_hotkeys(void);
static void ungrab_all(void);
static void quit(void);
static void signal_handler(int sig);
static void spawn(const Arg *arg);

xcb_connection_t *dpy;
char **eyie_envp;
xcb_window_t root_window;
xcb_key_symbols_t *symbols;

#include "config.h"

int main(int argc, char **argv, char** envp)
{
	/* Make our envp available to the hooks */
	eyie_envp = envp;

	/* X11 connection variables used to grab keys */
	xcb_setup_t const *setup;
	xcb_screen_t *screen;

	/* X11 Event handling */
	xcb_keycode_t *keycode;
	xcb_generic_event_t *generic_event;
	xcb_key_press_event_t *keypress_event;
	xcb_keysym_t keysym = XCB_NO_SYMBOL;
	uint16_t modfield;
	uint8_t event_type;

	/* Temporary variables */
	int i;

	/* First of all, connect to the X11 server. */
	dpy = xcb_connect(NULL, NULL);
	if (!dpy)
		return 1;

	/* To be able to efficiently grab keys, we have to
	 * grab them from the root window, so we need to
	 * get this root window before grabbing things.
	 */
	setup = xcb_get_setup(dpy);

	screen = xcb_setup_roots_iterator(setup).data;
	root_window = screen->root;

	symbols = xcb_key_symbols_alloc(dpy);

	signal(SIGINT, signal_handler);
	signal(SIGHUP, signal_handler);
	signal(SIGTERM, signal_handler);

	grab_hotkeys();

	xcb_flush(dpy);

	while(1) {
		generic_event = xcb_wait_for_event(dpy);
		event_type = XCB_EVENT_RESPONSE_TYPE(generic_event);
		switch(event_type) {
			case XCB_KEY_PRESS:
			case XCB_KEY_RELEASE:
				keypress_event = (xcb_key_press_event_t *) generic_event;
				keycode = &keypress_event->detail;
				modfield = keypress_event->state;
				keysym = xcb_key_symbols_get_keysym(symbols, *keycode, 0);

				for (i = 0; i < (sizeof(hotkeys)/sizeof(hotkey)); ++i) {
					if (keysym == hotkeys[i].key
					    && modfield == hotkeys[i].modfield
					    && event_type == hotkeys[i].fire_event)

						hotkeys[i].func(&hotkeys[i].arg);
				}
	/*			xcb_flush(dpy);*/
				free(generic_event);
				break;
			case XCB_MAPPING_NOTIFY:
				ungrab_all();
				xcb_key_symbols_free(symbols);
				symbols = xcb_key_symbols_alloc(dpy);
				grab_hotkeys();
			default:
				break;
		}
	}
	/* We will never reach this */
	quit();
	return 0;
}

void grab_hotkeys()
{
	xcb_generic_error_t *err;
	xcb_keycode_t *keycodes_list, *keycode;
	int i;

	/* Several (key) strokes in the keyboard can lead to the same (key) symbol.
	 * We want to grab all these keycodes.
	 */
	for (i = 0; i < (sizeof(hotkeys)/sizeof(hotkey)); ++i) {
		keycodes_list = xcb_key_symbols_get_keycode(symbols, hotkeys[i].key);
		if (keycodes_list != NULL)
			for (keycode = keycodes_list; *keycode != XCB_NO_SYMBOL; keycode++) {
				if (hotkeys[i].key == xcb_key_symbols_get_keysym(symbols, *keycode, 0))
					err = xcb_request_check(dpy, xcb_grab_key_checked(dpy, 1, root_window, hotkeys[i].modfield, *keycode, XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC));
			}
		free(keycodes_list);
	}

}

void ungrab_all()
{
	xcb_ungrab_key(dpy, XCB_GRAB_ANY, root_window, XCB_BUTTON_MASK_ANY);
}

void quit()
{
	ungrab_all();
	xcb_key_symbols_free(symbols);
	xcb_disconnect(dpy);
/*	printf("Exit !\n");*/
	exit(EXIT_SUCCESS);
}

void spawn(const Arg *arg)
{
	if (fork() == 0) {
		if (dpy)
			close(xcb_get_file_descriptor(dpy));
/*		close(stdin);
		close(stdout);
		close(stderr);*/
		setsid();
		execve(((char **)arg->v)[0], (char **)arg->v, eyie_envp);
		fprintf(stderr, "eyie: execve %s", ((char **)arg->v)[0]);
		perror(" failed");
		exit(EXIT_SUCCESS);
	}
}

void signal_handler(int sig)
{
	if (sig == SIGTERM || sig == SIGHUP || sig == SIGINT)
		quit();
}
